cursorline\_current.vim
=======================

This plugin tweaks the behaviour of the `'cursorline'` and `'cursorcolumn'`
options to enable them only in the current window, when not in insert mode,
and/or when Vim has focus.

In its default configuration, it essentially makes `'cursorline'` and
`'cursorcolumn'` follow the cursor around in normal mode as much as possible.
It uses each window's global value of both options as its default, so setting
each option in your `vimrc` before the plugin loads should do the trick.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
